import time
import datetime
import pyperclip

timeStamp = time.time()
timeStamp = datetime.datetime.fromtimestamp(
    timeStamp).strftime('%Y-%m-%d %H:%M:%S')
pyperclip.copy(timeStamp)
print(timeStamp)
